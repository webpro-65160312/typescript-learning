//tuples (need in order)
let myTuple: [number, boolean, string];
myTuple = [5, true, "god"];
console.log(myTuple);
//readonly
let strict: readonly [number, boolean, string];
strict = [5, true, "god"];
//named tuples
const graph: [x: number, y: number] = [55.2, 41.3];
//Destructuring Tuples
const [x, y] = graph;
