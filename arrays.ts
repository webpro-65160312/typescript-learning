//declare array and usage
const artist: string[] = [];
artist.push("Post Malone");
//readonly mode
const brand: readonly string[] = ["Bring me the horizon"];
//inference
const number = [1, 2, 4, 5]; //type 'number'
number.push(3);
