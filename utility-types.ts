//Partial (some)
interface Point {
  x: number;
  y: number;
}

let pointPart: Partial<Point> = {}; // `Partial` allows x and y to be optional
pointPart.x = 10;
//Required (all needed)
interface GCar {
  make: string;
  model: string;
  mileage?: number;
}
let mCar: Required<GCar> = {
  make: "Ford",
  model: "Focus",
  mileage: 12000, // `Required` forces mileage to be defined
};
//Record (shortcut to defining an object type and useful for declare many)
const nameAgeMap: Record<string, number> = {
  Alice: 21,
  Bob: 25,
};
