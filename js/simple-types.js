"use strict";
//explicit
const firstName = "john";
const age = 43;
const hasJob = true;
//implicit
const lastName = "josh";
const tall = 200;
const alive = false;
