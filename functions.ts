//common define function
function getTime(): number {
  return new Date().getTime();
}
console.log(getTime());
//void version
function printHello(): void {
  console.log("Hello!");
}
//parameter
function multiply(a: number, b: number) {
  return a * b;
}
// the `?` operator here marks parameter `c` as optional
function add(a: number, b: number, c?: number) {
  return a + b + (c || 0);
}
//default parameter's value
function pow(value: number, exponent: number = 10) {
  return value ** exponent;
}
//Named Parameters
function divide({ dividend, divisor }: { dividend: number; divisor: number }) {
  return dividend / divisor;
}
console.log(divide({ dividend: 10, divisor: 2 }));
//rest parameters
function plus(a: number, b: number, ...rest: number[]) {
  return a + b + rest.reduce((p, c) => p + c, 0);
}
console.log(plus(10, 10, 10, 10, 10));
//type alias
type Negate = (value: number) => number;

// in this function, the parameter `value` automatically gets assigned the type `number` from the type `Negate`
const negateFunction: Negate = (value) => value * -1;
