//keyof (extract the key type from an object type )
interface BigPerson {
  firstName: string;
  age: number;
}
// `keyof Person` here creates a union type of "name" and "age", other strings will not be allowed
function printPersonProperty(person: BigPerson, property: keyof BigPerson) {
  console.log(`Printing person property ${property}: "${person[property]}"`);
}
let myperson = {
  firstName: "Max",
  age: 27,
};
printPersonProperty(myperson, "firstName"); // Printing person property name: "Max"
//keyof with index signatures
type StringMap = { [key: string]: unknown };
// `keyof StringMap` resolves to `string` here
function createStringPair(property: keyof StringMap, value: string): StringMap {
  return { [property]: value };
}
