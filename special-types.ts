//can be 'any' type with no error
let u: any = true;
u = "string";
Math.round(u);
// console.log(u); //output is "string"
//type 'unknown' safer than 'any'
let w: unknown = 1;
w = "string";
w = {
  someMethod: () => {
    console.log("hello there");
  },
} as { someMethod: () => void };
if (typeof w === "object" && w !== null) {
  (w as { someMethod: Function }).someMethod();
}
