//object types
const car: { type: string; model: string; year: number } = {
  type: "Toyota",
  model: "Corolla",
  year: 2009,
};
//inference
car.type = "Ford";
console.log(car);
//optional properties (use '?')
const aircraft: { type: string; model: string; year?: number } = {
  type: "Jet",
  model: "Burst Nitro",
};
console.log(aircraft);
