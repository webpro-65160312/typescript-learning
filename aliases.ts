//type aliases is just custom types(name)
type CarYear = number;
type CarType = string;

type Car = {
  year: CarYear;
  type: CarType;
};

const carYear: CarYear = 2001;
const carType: CarType = "tesla";
const myCar: Car = {
  year: carYear,
  type: carType,
};
console.log(myCar);
