//casting with 'as'
let str: unknown = "hello";
console.log((str as string).length);
//casting with '<>'
let unk: unknown = 555;
console.log(<string>unk);
