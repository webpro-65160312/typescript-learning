//enum is represents a group of constants
enum CardinalDirections {
  North,
  East,
  South,
  West,
}
let currentDirection = CardinalDirections.North;
console.log(currentDirection);
//fully initialized (number | string)
//just in case not recommend to mix together
enum StatusCodes {
  NotFound = "test",
  Success = 200,
  Accepted = 202,
  BadRequest = 400,
}
console.log(StatusCodes.NotFound);
