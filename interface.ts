//interface is just pre-object
interface Rectangle {
  height: number;
  width: number;
}

const myRectangle: Rectangle = {
  height: 100,
  width: 200,
};
console.log(myRectangle);
