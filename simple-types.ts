//explicit
const firstName: string = "john";
const age: number = 43;
const hasJob: boolean = true;
//implicit
const lastName = "josh";
const tall = 200;
const alive = false;
//unable to infer
const json = JSON.parse("55");
console.log(json);
